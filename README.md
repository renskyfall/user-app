# user-app

How to use:

-You can copy User.sql in your local Mysql database
-After you run the database

```
$ go run main.go
```


API :

Start
```
Get : http://localhost:8089/api/start
```
Read By Id
```
Get : http://localhost:8089/api/user/read/:id
```

Read All
```
Get : http://localhost:8089/api/user/readAll
```

Create
```
Post : http://localhost:8089/api/user/

Json Body:
{   
    "username": "testing1",
    "password": "asdasdasd",
    "name": "asdasdasdsad"
}
```

Delete By Id
```
DELETE : http://localhost:8089/api/user/read/:id
```

